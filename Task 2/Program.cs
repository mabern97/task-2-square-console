﻿using System;

namespace Task_2
{
    class Program
    {
        const char SQUARE_LETTER = '#';
        const int SQUARE_SIZE = 5;

        /*
         * Initializes the program and starts the
         * logic of requesting user input and then
         * rendering either a Square or Rectangle
         * based on what the user's parameters are.
         */
        static void Main(string[] args)
        {
            Begin();

            bool restart = true;

            while (restart)
                restart = RequestRestart();
        }

        /*
         * Clear the console window and request
         * a width and a height for the rendered
         * object, and then once the parameters
         * are entered, render the object in the
         * console window.
         */
        static void Begin()
        {
            Console.Clear();
            Console.WriteLine("Welcome to the square generator!");

            int width = ReceiveUserInput("Please enter the width of the square: "); // Request a width of the square/rectangle
            int height = ReceiveUserInput("Please enter the height of the square: "); // Request a height of the square/rectangle

            GenerateSquare(width, height); // Render the square, arguments given are the width and height of the square/rectangle
        }

        /*
         * This method is in charge of getting input
         * from the user and attempt to convert the
         * input to a number.
         *
         * The method will check for the following:
         *  - The entered input is a number
         *  - The entered number is positive
         *
         * If the input entered is not valid, the
         * program will simply ask for the input
         * again and again until the user enters
         * a value that is valid.
         */
        static int ReceiveUserInput(string question)
        {
        request:
            SetConsoleColor(null);
            Console.Write(question);

            int theSize = SQUARE_SIZE;
            string userSize = Console.ReadLine();

            try
            {
                theSize = Convert.ToInt32(userSize);

                // If the given input is negative, throw an error about invalid range.
                if (theSize <= 0)
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception ex)
            {
                SetConsoleColor(ConsoleColor.Red);
                Console.WriteLine($"The given input is not valid, please try again. (Error: {ex.Message})");
                goto request;
            }

            SetConsoleColor(null);
            return theSize;
        }

        /*
         * Once the object is rendered in the console window
         * ask the user if they wish to render a new object
         * or if they wish to terminate the program
         */
        static bool RequestRestart()
        {
            response:
            Console.Write("Would you like to draw a new object (y=yes/n=no)? ");
            string answer = Console.ReadLine().ToLower();

            bool restart = true;

            switch(answer)
            {
                case "yes":
                    Begin();
                    break;
                case "y":
                    Begin();
                    break;
                case "no":
                    restart = false;
                    break;
                case "n":
                    restart = false;
                    break;
                default:
                    goto response;
            }

            return restart;
        }

        /*
         * Render the object (Either a square or a rectangle)
         * in the console window via use of two for-loops and
         * a conditional statement.
         */
        static void GenerateSquare(int width, int height)
        {
            SetConsoleColor(ConsoleColor.DarkYellow);
            Console.Write("Rendered object: ");
            if (width == height)
            {
                Console.WriteLine("Square");
            } else
            {
                Console.WriteLine("Rectangle");
            }

            SetConsoleColor(ConsoleColor.DarkGreen);
            for (int i = 0; i < height; i++) // For-loop: Generates all rows of the square/rectangle
            {
                Console.Write(SQUARE_LETTER);
                for (int j = 1; j < (width - 1); j++) // For-loop: Generates each column of the square/rectangle
                {
                    if (i == 0 || i == (height - 1)) // If: If the row is 0 or the height-1, then fill in the square with the specific character, else leave it empty
                    {
                        Console.Write($" {SQUARE_LETTER}");
                    } else
                    {
                        Console.Write("  ");
                    }
                }

                Console.WriteLine($" {SQUARE_LETTER}");
            }
            SetConsoleColor(null);
        }

        /*
         * Sets the color of text shown in the console window.
         * If no color is given, then by default the console
         * text color will be set to white.
         */
        static void SetConsoleColor(ConsoleColor? color)
        {
            if (color.HasValue)
            {
                Console.ForegroundColor = color.Value;
                return;
            }

            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
