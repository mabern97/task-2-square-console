Type:    Console Application

Allows the user to enter a number to display a square of that size. The square can be made of any character that you choose. (# or * is probably a good choice)

Requirements: 

·       Draw a square in the console

·       The square must be the size input by the user

Optional upgrade requirements:

·       Allow a user to print a square or a rectangle

Purpose: Learn C#: Common Control Structures and Variables

Weight: Basic